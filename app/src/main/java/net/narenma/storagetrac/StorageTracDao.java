package net.narenma.storagetrac;

import java.util.List;
import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.paging.DataSource;

@Dao
public interface StorageTracDao {
    @Query("SELECT * FROM storagetrac ORDER BY time DESC")
    DataSource.Factory<Integer, StorageTrac> getAll();

    @Query("SELECT * FROM storagetrac WHERE uid IN (:uids) ORDER BY time DESC")
    List<StorageTrac> loadAllByIds(int[] uids);

    //@Query("SELECT * FROM storagetrac WHERE changelog LIKE '%:query%'")
    //List<StorageTrac> findByName(String query);

    @Query("SELECT count(*) FROM storagetrac")
    int count();
    
    @Insert
    void insert(StorageTrac row);

    @Insert
    void insertAll(StorageTrac... rows);

    @Delete
    void delete(StorageTrac row);
}
