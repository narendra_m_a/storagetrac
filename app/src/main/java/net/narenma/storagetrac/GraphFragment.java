/*
 *  StorageTrac application - keeps track of the external storage usage
 *  Copyright (C) 2012 Narendra M.A.
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.narenma.storagetrac;

import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.MotionEvent;
import android.widget.Button;
import android.widget.Toast;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.core.view.GestureDetectorCompat;
import androidx.lifecycle.ViewModelProviders;
import androidx.lifecycle.Observer;
import androidx.paging.PagedList;
import androidx.annotation.Nullable;

import java.util.*;
import java.lang.Math;

import static android.view.GestureDetector.SimpleOnGestureListener;

public class GraphFragment extends Fragment
    implements RecyclerView.OnItemTouchListener,
               View.OnClickListener {

    private OnFragmentInteractionListener listenerActivity;

    private DatabaseViewModel viewModel;
    private RecyclerView recyclerView;
    private RecyclerViewAdapter viewAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private GestureDetectorCompat gestureDetector;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment GraphFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static GraphFragment newInstance(String param1, String param2) {
        GraphFragment fragment = new GraphFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);

        return fragment;
    }

    public GraphFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setRetainInstance(true);

        if (savedInstanceState != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setRetainInstance(true);
        // Inflate the layout for this fragment
        View retView = inflater.inflate(R.layout.fragment_graph, container, false);

        recyclerView = (RecyclerView)retView.findViewById(R.id.change_list_view);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        recyclerView.setHasFixedSize(true);
        // use a linear layout manager
        layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), LinearLayoutManager.VERTICAL));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        // specify an adapter
        viewAdapter = new RecyclerViewAdapter();
        recyclerView.setAdapter(viewAdapter);
        recyclerView.addOnItemTouchListener(this);
        gestureDetector = new GestureDetectorCompat(getContext(), new RecyclerViewOnGestureListener());

        // Setup view model
        viewModel = ViewModelProviders.of(this).get(DatabaseViewModel.class);
        viewModel.getData().observe(getActivity(), new Observer<PagedList<StorageTrac>>() {
                @Override
                public void onChanged(@Nullable PagedList<StorageTrac> items) {
                    //if (items != null) Log.d("StorageTracModel", "length " + items.size());
                    viewAdapter.submitList(items);
                }
            });
        
        return retView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            listenerActivity = (OnFragmentInteractionListener) context;
        } else {
            Log.e("onAttach", context.toString()
                  + " must implement OnFragmentInteractionListener");
            listenerActivity = null;
        }
    }
    
    @Override
    public void onClick(View view) {
        // Assume only recycler view children
        int idx = recyclerView.getChildAdapterPosition(view);
        if (idx != RecyclerView.NO_POSITION) {
            viewAdapter.notifySelection(idx);
            // Callback to activity
            listenerActivity.onFragmentInteraction(viewAdapter.getCurrentList().get(idx));
        } else {
            viewAdapter.clearSelection();
        }
    }
    
    // Recyclerview touch events
    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        gestureDetector.onTouchEvent(e);
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {
    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean b) {
    }
    
    @Override
    public void onDetach() {
        super.onDetach();
        listenerActivity = null;
    }

    @Override
    public void onViewCreated (View view, Bundle savedInstanceState) {
        // Setup navigation callbacks
    }

    @Override
    public void onSaveInstanceState(final Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(StorageTrac trac);
    }

    // Click listener
    private class RecyclerViewOnGestureListener extends SimpleOnGestureListener {
        @Override
        public boolean onSingleTapConfirmed(MotionEvent e) {
            View view = recyclerView.findChildViewUnder(e.getX(), e.getY());
            onClick(view);
            return super.onSingleTapConfirmed(e);
        }
    }
}
