/*
 *  StorageTrac application - keeps track of the external storage usage
 *  Copyright (C) 2018 Narendra M.A.
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.narenma.storagetrac;

import android.content.Context;
import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.ExpandableListView;
import android.widget.ExpandableListAdapter;
import androidx.annotation.Nullable;
import android.os.Bundle;
import android.os.Parcelable;
import java.util.Date;
import java.text.SimpleDateFormat;

public class ChangeLogFragment extends Fragment {
    private StorageTrac item;
    ExpandableListView listView;
    ExpandableListAdapter listAdapter;
    
    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     */
    public static ChangeLogFragment newInstance(StorageTrac item) {
        ChangeLogFragment fragment = new ChangeLogFragment();
        Bundle args = new Bundle();
        args.putSerializable("item", item);
        fragment.setArguments(args);
        return fragment;
    }
    
    public ChangeLogFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            this.item = (StorageTrac)getArguments().getSerializable("item");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View retView = inflater.inflate(R.layout.fragment_changelog, container, false);
        listView = (ExpandableListView)retView.findViewById(R.id.changelog_list);
        listAdapter = new MyExpandableListAdapter(getContext(), item);
        listView.setAdapter(listAdapter);
        // Set title
        TextView title = (TextView)retView.findViewById(R.id.changelog_title);
        Date timeStamp = new Date(item.getTimeStamp());
        title.setText(new SimpleDateFormat("MM-dd-yyyy HH:mm").format(timeStamp));
        TextView detail = (TextView)retView.findViewById(R.id.changelog_detail);
        String detailText = "Storage usage ";
        if (item.getDelta() == 0)
            detailText += "did not change";
        else {
            if (item.getDelta() < 0)
                detailText += "decreased by ";
            else
                detailText += "increased by ";
            detailText += DatabaseViewModel.convertToStorageUnits(item.getDelta());
        }
        detail.setText(detailText);
        return retView;
    }
}

