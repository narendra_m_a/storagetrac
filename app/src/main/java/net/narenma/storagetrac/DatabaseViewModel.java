package net.narenma.storagetrac;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.paging.LivePagedListBuilder;
import androidx.paging.PagedList;
import android.content.Context;
import android.os.AsyncTask;
import android.app.Application;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.text.DecimalFormat;

public class DatabaseViewModel extends AndroidViewModel {
    private final LiveData<PagedList<StorageTrac>> data;
    private AppDatabase appDatabase;
    
    public DatabaseViewModel(Application application) {
        super(application);
        appDatabase = AppDatabase.getDatabase(this.getApplication());
        data = new LivePagedListBuilder<>(appDatabase.storageTracDao().getAll(), 20)
            .build();
    }

    public LiveData<PagedList<StorageTrac>> getData() {
        return data;
    }

    public LiveData<PagedList<StorageTrac>> search(String query) {
        return data;
    }
    
    public void addData(final StorageTrac trac) {
        new addAsyncTask(appDatabase).execute(trac);
    }

    private static class addAsyncTask extends AsyncTask<StorageTrac, Void, Void> {
        private AppDatabase db;

        addAsyncTask(AppDatabase appDatabase) {
            db = appDatabase;
        }

        @Override
        protected Void doInBackground(final StorageTrac... params) {
            db.storageTracDao().insert(params[0]);
            return null;
        }

    }
    
    // Format string to be more readable
    public static String convertToStorageUnits(double value) {
        String retValue;
        long scaling;
        String suffix;
        double absVal = Math.abs(value);

        if ((absVal / 1000) < 1) { // Under a KB, keep as is
            scaling = 1;
            suffix = "B";
        } else if ((absVal / 1000000) < 1) { // KB
            scaling = 1000;
            suffix = "KB";
        } else if ((absVal / 1000000000) < 1) { // MB
            scaling = 1000000;
            suffix = "MB";
        } else { // GB
            scaling = 1000000000;
            suffix = "GB";
        }

        retValue = new DecimalFormat("#.#").format((value / scaling)) + suffix;
        return retValue;
    }
}
