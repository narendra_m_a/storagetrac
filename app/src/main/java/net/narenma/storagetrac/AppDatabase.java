package net.narenma.storagetrac;

import android.content.Context;
import androidx.room.Room;
import androidx.room.Database;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

@Database(entities = {StorageTrac.class}, version = 1, exportSchema = false)
@TypeConverters(DatabaseConverters.class)
public abstract class AppDatabase extends RoomDatabase {
    private static AppDatabase INSTANCE;

    public static AppDatabase getDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE =
                Room.databaseBuilder(context.getApplicationContext(), AppDatabase.class, "storagetrac")
                .build();
        }
        return INSTANCE;
    }

    public abstract StorageTracDao storageTracDao();
}
