/*
 *  StorageTrac application - keeps track of the external storage usage
 *  Copyright (C) 2012 Narendra M.A.
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.narenma.storagetrac;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Parcelable;
import android.preference.PreferenceManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.core.app.ActivityCompat; //androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat; //androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity; //androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar; //androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.Toast;
import java.util.List;

public class GraphActivity extends AppCompatActivity
        implements GraphFragment.OnFragmentInteractionListener {

    int messageIndex;
    public static final String TAB_NAME_EXT_STORAGE = "External";
    public static final String SHOW_HELP_TAG = "showHelp";
    private static final int PERMISSION_STORAGE = 0;
    private static final int PERMISSION_BOOT = 1;
    private String interval;
    private Spinner durationSel;
    private boolean forceSettings = false;
    private boolean helpExit = false;
    private boolean showTips = false;
    private boolean alarmEnabled = false;
    private boolean reInst = false;
    private boolean serviceBound = false;

    @Override
    public void onCreate(Bundle savedInstance) {
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
        super.onCreate(savedInstance);
        setContentView(R.layout.graph);
        
        Toolbar toolbar = (Toolbar) findViewById(R.id.app_toolbar);
        setSupportActionBar(toolbar);
        
        if (savedInstance == null) {
            // Instance of first fragment
            GraphFragment firstFragment = new GraphFragment();
            // Add Fragment to FrameLayout, using FragmentManager
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.add(R.id.fl_container, firstFragment);
            ft.commit();
        }

        // Setup debug logging
        SettingsActivity.ENABLE_DEBUG = pref.getBoolean(SettingsActivity.ENABLE_DEBUG_KEY, false);

        // Help on first
        showTips = pref.getBoolean(SHOW_HELP_TAG, true);
        if (showTips) {
            showHelp();
        }

        // Start service
        alarmEnabled = pref.getBoolean(SettingsActivity.ALARM_RUNNING_KEY, false);
        reInst = false;
        if (savedInstance != null) {
            Parcelable spinner;
            reInst = savedInstance.getBoolean(SettingsActivity.ALARM_RUNNING_KEY, false);
            interval = savedInstance.getString("interval", "Day");
            spinner = savedInstance.getParcelable("spinner");
            if (spinner != null)
                durationSel.onRestoreInstanceState(spinner);
        }

        // Check permissions
        if (askPermission(Manifest.permission.READ_EXTERNAL_STORAGE, PERMISSION_STORAGE)
                == PackageManager.PERMISSION_GRANTED) {
            // Permission has already been granted
            Log.d("permission", "storage permission already granted");
            startFileObserve();
        }
        askPermission(Manifest.permission.RECEIVE_BOOT_COMPLETED, PERMISSION_BOOT);
    }

    private int askPermission(String permission, int code) {
        if (ContextCompat.checkSelfPermission(this,
                permission)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    permission)) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
                Toast.makeText(this, "Permission denied, cannot record data!", Toast.LENGTH_SHORT).show();
                Log.e("permission", "denied !");
            } else {
                // No explanation needed; request the permission
                ActivityCompat.requestPermissions(this,
                        new String[]{permission},
                        code);
            }
            return PackageManager.PERMISSION_DENIED;
        } else {
            return PackageManager.PERMISSION_GRANTED;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.d("permission", "storage permission granted");
                    startFileObserve();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(this, "Permission denied, cannot record data!", Toast.LENGTH_SHORT).show();
                    Log.e("permission", "denied in callback!");
                }
                return;
            }
        }
    }

    // Start file observers, called after permission granted
    private void startFileObserve() {
        if (alarmEnabled && !reInst) {
            Intent serviceIntent = new Intent(this, FileObserverService.class);
            serviceIntent.setAction(Intent.ACTION_MAIN);
            startService(serviceIntent);
        }
    }

    // Menu creating and handling
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.graph_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // TODO
        switch (item.getItemId()) {
        case R.id.graph_refresh:
            refreshGraph();
            return true;
        case R.id.graph_settings:
            showSettings();
            return true;
        case R.id.help:
            showHelp();
            return true;
        }

        return false;
    }

    // Refresh graph on resume
    @Override
    public void onResume() {
        super.onResume();
        //refreshGraph(false, interval);
    }

    // Refresh all graphs
    private void refreshGraph() {
        // Trigger collection
        Intent triggerCollect = new Intent(this, FileObserverService.class);
        triggerCollect.setAction(Intent.ACTION_VIEW);
        startService(triggerCollect);
    }

    // Goto settings menu
    public void showSettings() {
        Intent show = new Intent(this, SettingsActivity.class);
        startActivity(show);
    }

    @Override
    public void onFragmentInteraction(StorageTrac trac) {
        ChangeLogFragment fragment = ChangeLogFragment.newInstance(trac);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fl_container, fragment);
        ft.addToBackStack(null);
        ft.commit();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    @Override
    public void onPause() {
        super.onPause();
        // Store flag
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor edit = pref.edit();
        edit.putBoolean(SHOW_HELP_TAG, false);
        edit.commit();
        showTips = false;
    }

    @Override
    protected void onSaveInstanceState(Bundle out) {
        super.onSaveInstanceState(out);
        out.putBoolean(SettingsActivity.ALARM_RUNNING_KEY, alarmEnabled);
        out.putString("interval", interval);
    }

    @Override
    public boolean onSupportNavigateUp() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayShowHomeEnabled(false);
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setDisplayShowHomeEnabled(false);
        }
        super.onBackPressed();
    }
    
    // Help popup
    private void showHelp() {
        final HelpFragment help = new HelpFragment();
        help.show(getSupportFragmentManager(), "help");
    }
}
