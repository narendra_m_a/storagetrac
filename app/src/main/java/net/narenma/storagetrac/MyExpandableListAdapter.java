package net.narenma.storagetrac;

import android.app.Service;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.Gravity;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ExpandableListView;
import android.widget.AbsListView;
import android.widget.TextView;
import android.widget.BaseExpandableListAdapter;
import android.content.Context;
import android.content.res.Resources;
import android.text.TextUtils;
import android.graphics.Typeface;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.HashMap;

// Adapter for expandable list view
public class MyExpandableListAdapter extends BaseExpandableListAdapter {
    // Sample data set.  children[i] contains the children (String[]) for groups[i].
    private List<String> groups;
    private List<List<String>> children;
    private boolean noItems = false;
    private Context ctx;
    private Resources resources;

    public MyExpandableListAdapter(Context c, StorageTrac trac) {
        groups = new ArrayList();
        children = new ArrayList();
        ctx = c;
        resources = ctx.getResources();
        addToList(resources.getString(R.string.files_created), trac.getFilesCreated());
        addToList(resources.getString(R.string.files_modified), trac.getFilesModified());
        addToList(resources.getString(R.string.files_deleted), trac.getFilesDeleted());
    }

    private void addToList(String group, List<String> files) {
        addGroup(group);
        for (String path : files) {
            addChild(path);
        }
    }
    
    public void addGroup(String groupName) {
        groups.add(groupName);
        children.add(new ArrayList<String>());
    }

    public void addChild(String childName) {
        // Add to last created group
        children.get(children.size() - 1).add(childName);
    }

    public void addChild(int group, String childName) {
        // Add to last created group
        children.get(group).add(childName);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return children.get(groupPosition).get(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return children.get(groupPosition).size();
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild,
                             View convertView, ViewGroup parent) {
        final String childText = (String)getChild(groupPosition, childPosition).toString();
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.ctx
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.list_item, null);
        }
        TextView textView = (TextView) convertView
                .findViewById(R.id.changelog_item_title);
        textView.setText(childText);
        return textView;
    }

    @Override
    public Object getGroup(int groupPosition) {
        String ret = groups.get(groupPosition) + " (" + getChildrenCount(groupPosition) + ")";
        return ret;
    }

    @Override
    public int getGroupCount() {
        return groups.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView,
                             ViewGroup parent) {
        String listTitle = (String)getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.ctx.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.list_group, null);
        }
        TextView listTitleTextView = (TextView) convertView
            .findViewById(R.id.changelog_group_title);
        listTitleTextView.setTypeface(null, Typeface.BOLD);
        listTitleTextView.setText(listTitle);
        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    public boolean isEmpty() {
        return noItems;
    }

    public void clear() {
        int i = 0;
        for (String s : groups) {
            children.get(i++).clear();
        }
        children.clear();
        groups.clear();
    }

}
