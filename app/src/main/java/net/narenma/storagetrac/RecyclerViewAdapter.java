/*
 *  StorageTrac application - keeps track of the external storage usage
 *  Copyright (C) 2018 Narendra M.A.
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.narenma.storagetrac;

import android.view.LayoutInflater;
import android.widget.TextView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.DiffUtil;
import androidx.paging.PagedListAdapter;
import java.util.List;
import java.util.Date;
import java.text.SimpleDateFormat;

public class RecyclerViewAdapter
    extends PagedListAdapter<StorageTrac, RecyclerViewAdapter.RecyclerViewHolder> {
    private int selectedIdx;

    public RecyclerViewAdapter() {
        super(DIFF_CALLBACK);
        selectedIdx = -1;
    }

    @Override
    public RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new RecyclerViewHolder(LayoutInflater.from(parent.getContext())
                                      .inflate(R.layout.recycler_item, parent, false));
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolder holder, int position) {
        StorageTrac tracModel = getItem(position);
        if (holder != null) {
            holder.bindTo(tracModel, position, selectedIdx);
        } else {
            holder.clear();
        }
    }
    /*
    @Override
    public int getItemCount() {
        return tracModelList.size();
    }

    public void addItems(List<StorageTrac> tracModelList) {
        this.tracModelList = tracModelList;
        notifyDataSetChanged();
    }

    public StorageTrac getItem(int idx) {
        return tracModelList.get(idx);
    }
    */
    public void notifySelection(int idx) {
        selectedIdx = idx;
        notifyDataSetChanged();
    }

    public void clearSelection() {
        selectedIdx = -1;
        notifyDataSetChanged();
    }

    public static final DiffUtil.ItemCallback<StorageTrac> DIFF_CALLBACK =
        new DiffUtil.ItemCallback<StorageTrac>() {
            @Override
            public boolean areItemsTheSame(StorageTrac lhs, StorageTrac rhs) {
                // User properties may have changed if reloaded from the DB, but ID is fixed
                return lhs.getId() == rhs.getId();
            }
            @Override
            public boolean areContentsTheSame(StorageTrac lhs, StorageTrac rhs) {
                // NOTE: if you use equals, your object must properly override Object#equals()
                // Incorrectly returning false here will result in too many animations.
                return lhs.equals(rhs);
            }
        };
        
    static class RecyclerViewHolder extends RecyclerView.ViewHolder {
        private TextView logTextView;
        private TextView dateTextView;
        private TextView createdView;
        private TextView modifiedView;
        private TextView deletedView;

        RecyclerViewHolder(View view) {
            super(view);
            dateTextView = (TextView) view.findViewById(R.id.item_date_view);
            logTextView = (TextView) view.findViewById(R.id.item_size_view);
            createdView = (TextView) view.findViewById(R.id.item_created_view);
            modifiedView = (TextView) view.findViewById(R.id.item_modified_view);
            deletedView = (TextView) view.findViewById(R.id.item_deleted_view);
        }

        public void bindTo(StorageTrac tracModel, int position, int selected) {
            String arrow;
            if (tracModel.getDelta() < 0) arrow = "\u21E9";
            else arrow = "\u21E7";
            logTextView.setText(arrow + " "
                                + DatabaseViewModel.convertToStorageUnits(tracModel.getDelta())
                                + ", "
                                + DatabaseViewModel.convertToStorageUnits(tracModel.getUsage()));
            Date timeStamp = new Date(tracModel.getTimeStamp());
            dateTextView.setText(new SimpleDateFormat("MM-dd-yyyy HH:mm").format(timeStamp));
            createdView.setText(Integer.toString(tracModel.getCreated()));
            createdView.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, tracModel.getCreated()));
            modifiedView.setText(Integer.toString(tracModel.getModified()));
            modifiedView.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, tracModel.getModified()));
            deletedView.setText(Integer.toString(tracModel.getDeleted()));
            deletedView.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, tracModel.getDeleted()));
            // Selection
            if (position == selected) itemView.setActivated(true);
            else itemView.setActivated(false);
            itemView.setTag(tracModel);
        }

        public void clear() {
            dateTextView.setText(null);
            logTextView.setText(null);
            createdView.setText(null);
            modifiedView.setText(null);
            deletedView.setText(null);
        }
    }
}
