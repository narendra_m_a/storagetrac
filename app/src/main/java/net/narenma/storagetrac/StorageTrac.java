package net.narenma.storagetrac;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.ColumnInfo;
import java.util.List;
import java.io.Serializable;

// Class to store data required for a point in the Graph
@Entity
public class StorageTrac implements Serializable {
    @PrimaryKey(autoGenerate = true)
    public int uid;
    @ColumnInfo(name = "time")
    public long timeStamp;
    @ColumnInfo(name = "usage")
    private final long usage;
    @ColumnInfo(name = "delta")
    private final long delta;
    @ColumnInfo(name = "modified")
    private final int modified;
    @ColumnInfo(name = "created")
    private final int created;
    @ColumnInfo(name = "deleted")
    private final int deleted;
    @ColumnInfo(name = "filesCreated")
    private final List<String> filesCreated;
    @ColumnInfo(name = "filesModified")
    private final List<String> filesModified;
    @ColumnInfo(name = "filesDeleted")
    private final List<String> filesDeleted;

    public StorageTrac(long timeStamp, long usage, long delta,
                       int created, int modified, int deleted,
                       List<String> filesCreated, List<String> filesModified,
                       List<String> filesDeleted) {
        this.timeStamp = timeStamp;
        this.usage = usage;
        this.delta = delta;
        this.created = created;
        this.modified = modified;
        this.deleted = deleted;
        this.filesCreated = filesCreated;
        this.filesModified = filesModified;
        this.filesDeleted = filesDeleted;
    }

    public int getId() { return uid; }
    public long getTimeStamp() { return timeStamp; }
    public long getUsage() { return usage; }
    public long getDelta() { return delta; }
    public int getCreated() { return created; }
    public int getModified() { return modified; }
    public int getDeleted() { return deleted; }
    public List<String> getFilesCreated() { return filesCreated; }
    public List<String> getFilesModified() { return filesModified; }
    public List<String> getFilesDeleted() { return filesDeleted; }
}
