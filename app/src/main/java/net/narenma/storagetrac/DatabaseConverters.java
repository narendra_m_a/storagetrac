package net.narenma.storagetrac;

import androidx.room.TypeConverter;
import android.util.Log;
import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONTokener;
import org.json.JSONException;
import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
import java.util.Date;

public class DatabaseConverters {
    @TypeConverter
    public static Date fromTimestamp(Long value) {
        return value == null ? null : new Date(value);
    }

    @TypeConverter
    public static Long dateToTimestamp(Date date) {
        return date == null ? null : date.getTime();
    }
    @TypeConverter
    public static List<String> fromJsonToList(String json) {
        try {
            JSONObject obj = (JSONObject)new JSONTokener(json).nextValue();
            JSONArray arr = obj.getJSONArray("list");
            ArrayList<String> ret = new ArrayList<String>();
            if (arr != null) {
                for (int i = 0; i < arr.length(); i++)
                    ret.add(arr.getString(i));
            }
            return ret;
        } catch (JSONException e) {
            Log.e("fromJsonToList", "JSON parse error: " + e);
            return new ArrayList<String>(); // empty
        }
    }

    @TypeConverter
    public static String fromListToJson(List<String> items) {
        JSONObject ret = new JSONObject();
        JSONArray arr = new JSONArray(items);
        try {
            ret.put("list", arr);
        } catch (JSONException e) {
            Log.e("fromListToJson", "JSON error: " + e);
            return "";
        }
        return ret.toString();
    }
}
