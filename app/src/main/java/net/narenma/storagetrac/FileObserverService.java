/*
 * SDCardTrac application - keeps track of the /sdcard usage
 * Copyright (C) 2012 Narendra M.A.
 */

package net.narenma.storagetrac;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.lang.CharSequence;
import android.app.Service;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Intent;
import android.os.Binder;
import android.os.Environment;
import android.os.FileObserver;
import android.os.IBinder;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import android.app.NotificationChannel;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class FileObserverService extends Service {

    private List <UsageFileObserver> fobsList; // List of FileObserver objects, must always be referenced by Service in order for the hooks to work
    private static final int FOBS_EVENTS_TO_LISTEN =
            (FileObserver.MODIFY | FileObserver.CREATE | FileObserver.DELETE |
                    FileObserver.MOVED_FROM | FileObserver.MOVED_TO); // Event mask
    public static final int ONGOING_NOTIFICATION_ID = 2929;
    private static final String CHANNEL_ID = "StorageTrac";
    private static final String PREV_USAGE_KEY = "PrevUsage";
    private int numObs;
    private String basePath;
    private static boolean booted;
    private static long prevUsage;
    private AppDatabase appDatabase;
    private NotificationManagerCompat notificationManager;
    private SharedPreferences preferences;
    
    // Data-structure of a file event
    public class ObservedEvent {
        public String filePath = "";
        public int eventMask = 0;
        public boolean duplicate = false;

        public boolean compareWith(ObservedEvent i) {
            return (filePath.equals(i.filePath) && (eventMask == i.eventMask));
        }

        public String toString() {
            return filePath + ":" + eventMask;
        }
    }

    private ConcurrentLinkedQueue <ObservedEvent> eventsList; // Periodically re-created list of events

    // Binder to talk to periodic tracking activity
    public class TrackingBinder extends Binder {
        // Fetch events
        public ObservedEvent[] getAllEvents () {
            ObservedEvent[] retEvents = (ObservedEvent[]) eventsList.toArray();
            eventsList.clear();
            return retEvents;
        }
        // Service handle
        public FileObserverService getService() {
            return FileObserverService.this;
        }
        // Enable/disable foreground
        public void foregroundService(boolean enable) {
            if (enable)
                runInForeground();
            else
                stopForeground(true);
        }
    }
    private final IBinder locBinder = new TrackingBinder();

    @Override
    public void onCreate() {
        // Create database handle
        appDatabase = AppDatabase.getDatabase(this.getApplication());
        booted = false;
        createNotificationChannel();
        preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
    }

    // Below is called on: 
    // 1. From activity starting up the whole thing
    // 2. From an alarm indicating to collect events
    // 3. Media remounted, to record delta size
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        // Decode the intent
        //Log.d(this.getClass().getName(), "Got intent : " + intent.getAction());
        if ((intent == null) || (intent.getAction().equals(Intent.ACTION_MAIN))) {
            if (!booted) {
                File baseDir = Environment.getExternalStorageDirectory();
                basePath = baseDir.getAbsolutePath();
                Log.d(this.getClass().getName(), "Creating the observers");
                numObs = 0;
                fobsList = new ArrayList<UsageFileObserver>();
                eventsList = new ConcurrentLinkedQueue<FileObserverService.ObservedEvent>();
                parseDirAndHook(baseDir, true);
                Log.d(this.getClass().getName(), "Done hooking all observers (" + numObs + " of them)"
                        + " starting at " + basePath);
                booted = true;
            }

            for (UsageFileObserver i : fobsList) {
                i.startWatching();
            }
            if (preferences.getBoolean(SettingsActivity.FOREGROUND_KEY, false))
                runInForeground();
            Log.d(this.getClass().getName(), "Started watching...");
        } else if (intent != null) { // Other actions
            if (intent.getAction().equals(Intent.ACTION_VIEW) && eventsList != null) { // Collect data
                Log.d(this.getClass().getName(), "Clearing " + eventsList.size() + " events");
                storeAllEvents(false);
            } else if (intent.getAction().equals(Intent.ACTION_SYNC)) {
                storeAllEvents(true);
            } else if (intent.getAction().equals(Intent.ACTION_DELETE)) {
                stopWatching();
            }
        }
        prevUsage = preferences.getLong(PREV_USAGE_KEY, 0);
        return START_STICKY; // Continue running after return
    }

    @Override
    public void onDestroy() {
        booted = false;
        stopWatching();
    }

    // Return handle to service so that tracker can collect events
    @Override
    public IBinder onBind(Intent intent) {
        return locBinder;
    }

    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        notificationManager = NotificationManagerCompat.from(getApplicationContext());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.channel_name);
            String description = getString(R.string.channel_description);
            int importance = NotificationManagerCompat.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            notificationManager.createNotificationChannel(channel);
        }
    }

    // Run in foreground
    private void runInForeground() {
        Intent notificationIntent = new Intent(this, GraphActivity.class);
        PendingIntent pendingIntent =
            PendingIntent.getActivity(this, 0, notificationIntent, 0);

        Notification notification =
            new NotificationCompat.Builder(this, CHANNEL_ID)
            .setContentTitle(getText(R.string.notification_title))
            .setContentText(getText(R.string.notification_message))
            .setSmallIcon(R.drawable.ic_stat_sd_card)
            .setContentIntent(pendingIntent)
            .setOnlyAlertOnce(true)
            .build();
        startForeground(ONGOING_NOTIFICATION_ID, notification);
    }
    
    // Recursively hookup the FileObserver's
    boolean parseDirAndHook (File fromDir, boolean hookObs)
    {
        boolean locError = false;

        try {
            if (hookObs) { // Place observer on even levels, this *doesnt* work, so place on all
                // The even level strategy would work if FileObserver operation was as it is documented
                // Observer will call queueEvent(filePath, eventMask) of this service
                UsageFileObserver currObs = new UsageFileObserver(fromDir.getPath(),
                        FOBS_EVENTS_TO_LISTEN, this);
                fobsList.add(currObs); // ID of this observer is the list index
                numObs++;
//        		Log.d(this.getClass().getName(), "Hooking to " + fromDir.getPath());
            }
        } catch (NullPointerException e) {
            locError = true;
            Log.e(this.getClass().getName(), "Error hooking to " + fromDir.getPath() +
                    "=" + e.toString());
        }
        //Log.d(this.getClass().getName(), "Descending into " + fromDir.getPath() + " files " + fromDir.listFiles());
        if (!locError && (fromDir != null) && (fromDir.listFiles() != null)) {
            for (File i : fromDir.listFiles()) {
                if (i.isDirectory()) {
                    // Recursive call
                    locError = parseDirAndHook(i, hookObs);
                }
            }
        }

        return locError;
    }

    // Stop watching
    private void stopWatching() {
        for (UsageFileObserver i : fobsList) {
            i.stopWatching();
        }
        Log.d(this.getClass().getName(), "Stopped watching...");
    }

    // Callback for event recording
    public void queueEvent(String filePath, int eventMask, UsageFileObserver fileObs) {
        ObservedEvent currEvent = new ObservedEvent();

        currEvent.filePath = filePath; currEvent.eventMask = eventMask;
        currEvent.duplicate = false; // Will be updated while reporting
        if (SettingsActivity.ENABLE_DEBUG)
            Log.d(getClass().getName(), "queueEvent " + currEvent.toString());
//        synchronized (syncEventsList) {
        eventsList.add(currEvent);
//		}

        // Look at any FileObserver manipulations needed due to file/directory updates
        // Hook up observer if a new directory is created
        if ((eventMask & FileObserver.CREATE) != 0 ||
                (eventMask & FileObserver.MOVED_TO) != 0) {
            File chkDir = new File(filePath);
            if (chkDir.isDirectory()) {
                parseDirAndHook(chkDir, true);
            }
            fobsList.get(fobsList.size() - 1).startWatching();
        }
    }

    // Consumer of eventsList
    public void storeAllEvents (boolean ignoreEvents) {
        if (eventsList == null) return;
        ObservedEvent [] tmpEv = eventsList.toArray(new ObservedEvent[eventsList.size()]);
        // Use synchronized version since queueEvent may be modifying the eventsList
        List <ObservedEvent> uniqEvents = Arrays.asList(tmpEv);
        //new ArrayList <ObservedEvent> ();

        if (!ignoreEvents) {
            // Return if no events and running foreground
            if (eventsList.isEmpty() &&
                preferences.getBoolean(SettingsActivity.FOREGROUND_KEY, false)) {
                Log.w(this.getClass().getName(), "No events yet!");
                return;
            }

            eventsList.clear();
        }

        // Check if storage is available
        String storeState = Environment.getExternalStorageState();
        if (!(storeState.equals(Environment.MEDIA_MOUNTED) ||
                storeState.equals(Environment.MEDIA_MOUNTED_READ_ONLY)))
            return;

        // Get the space data
        long totalSpace = Environment.getExternalStorageDirectory().getTotalSpace();
        long freeSpace = Environment.getExternalStorageDirectory().getFreeSpace();
        long usedSpace = (totalSpace - freeSpace);
        long deltaSpace = 0;
        if (prevUsage > 0) deltaSpace = usedSpace - prevUsage;
        else deltaSpace = 0;
        prevUsage = usedSpace;
        preferences.edit().putLong(PREV_USAGE_KEY, prevUsage).apply();
        int numModified = 0, numCreated = 0, numDeleted = 0;
        if (SettingsActivity.ENABLE_DEBUG)
            Log.d(getClass().getName(), "Total = " + totalSpace + ", free = " + freeSpace);

        // Store in database
        HashMap <String, Character> fileEvents = new HashMap<String, Character>();

        // Get most relevant event for a file
        for (ObservedEvent i : uniqEvents) {
            String filePath = i.filePath;
            boolean created = ((i.eventMask & FileObserver.CREATE) != 0);
            boolean deleted = ((i.eventMask & FileObserver.DELETE) != 0);
            boolean moved = ((i.eventMask & FileObserver.MOVED_TO) != 0);
            boolean modified = ((i.eventMask & FileObserver.MODIFY) != 0);
            char newVal = 0;

            if (fileEvents.containsKey(filePath)) {
                // Keep only the latest event flag
                switch ((char)fileEvents.get(filePath)) {
                    case 'C': // Was created
                        // Delete and move override
                        if (moved)
                            newVal = 'V';
                        if (deleted)
                            newVal = 'D';
                        break;
                    case 'D': // Was deleted
                        // Create overrides
                        if (created)
                            newVal = 'C';
                        break;
                    case 'V': // Was moved
                        // Delete and create overrides
                        if (deleted)
                            newVal = 'D';
                        if (created)
                            newVal = 'C';
                        break;
                    case 'M': // Modified
                        // All override
                        newVal = mapEventToChar(i.eventMask);
                        break;
                    default:
                        break;
                }
            } else {
                newVal = mapEventToChar(i.eventMask);
            }
            if (newVal != 0)
                fileEvents.put(filePath, newVal);
        }

        // Create message from map of file and events
        ArrayList<String> filesCreated, filesModified, filesDeleted;
        filesCreated = new ArrayList<String>();
        filesModified = new ArrayList<String>();
        filesDeleted = new ArrayList<String>();
        if (ignoreEvents) {
            filesModified.add("- External event -");
        } else {
            for (String k : fileEvents.keySet()) {
                if (SettingsActivity.ENABLE_DEBUG)
                    Log.d(getClass().getName(), "fileEvent: " + k);
                switch (fileEvents.get(k)) {
                case 'C': filesCreated.add(k); break;
                case 'M': filesModified.add(k); break;
                case 'D': filesDeleted.add(k); break;
                }
            }
        }

        numCreated = filesCreated.size();
        numModified = filesModified.size();
        numDeleted = filesDeleted.size();
        StorageTrac trac = new StorageTrac(System.currentTimeMillis(), usedSpace, deltaSpace,
                                           numCreated, numModified, numDeleted,
                                           filesCreated, filesModified, filesDeleted);
        new AddAsyncTask(appDatabase).execute(trac);
    }

    // Helper
    private char mapEventToChar(int eventMask) {
        char changeTag = 0;
        if ((eventMask & FileObserver.CREATE) != 0)
            changeTag = 'C';
        if ((eventMask & FileObserver.DELETE) != 0)
            changeTag = 'D';
        if ((eventMask & FileObserver.MOVED_TO) != 0)
            changeTag = 'V';
        if (changeTag == 0) {
            if (eventMask != 0)
                changeTag = 'M';
            else
                changeTag = 'N';
        }
        return changeTag;
    }

    private static class AddAsyncTask extends AsyncTask<StorageTrac, Void, Void> {
        private AppDatabase db;

        AddAsyncTask(AppDatabase appDatabase) {
            db = appDatabase;
        }

        @Override
        protected Void doInBackground(final StorageTrac... params) {
            db.storageTracDao().insert(params[0]);
            Log.d("AddAsyncTask", "inserting " + params[0]);
            return null;
        }

    }

}
